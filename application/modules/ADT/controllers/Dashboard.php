<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('adt_reports_model');
        $this->load->model('adt_sites_model');
    }
    public function index()
    {
        $data['adt_sites']  = $this->adt_sites_model->get_adt_sites_distribution_numbers(); 
        $data['title'] = 'WEB ADT Installed Dashboard';
        
        $this->load->view('facilities_view', $data);
    }
    public function home()
    {
        $data['title'] = 'WEB ADT Installed Dashboard';
        
        $this->load->view('home_view', $data);
    }
    public function backups($facility)
    {
        $data['title']            = 'ADT Backups';
        $data['backups']          = $this->adt_sites_model->get_adt_backups();
        $data['backups_versions'] = $this->adt_sites_model->get_adt_backups_versions();
        $data['no_backups']       = $this->adt_sites_model->get_no_backup();
        
        $this->load->view('backups_view', $data);
    }
    public function partner()
    {
        $data['partner_service'] = $this->adt_sites_model->get_partner_service();
        $data['title'] = 'ADT Partner';
         
        $this->load->view('partner_view', $data);
    }
    public function county()
    {
        $data['county_patients'] = $this->adt_sites_model->get_patients_by_county();
        $data['title'] = 'ADT County Distribution';
        
        $this->load->view('county_view', $data);
    }
    public function subcounty()
    {
        $data['subcounty_patients'] = $this->adt_sites_model->get_patients_by_subcounty();
        $data['title'] = 'ADT Subcounty Distribution';
        
        $this->load->view('subcounty_view', $data);
    }
    public function facilities()
    {
        $data['adt_sites']  = $this->adt_sites_model->get_adt_sites_distribution_numbers(); 
        $data['title'] = 'WEB ADT Installed Dashboard';
        
        $this->load->view('facilities_view', $data);
    }
    public function facility($facility)
    { 
        $data['facility']               = $this->adt_sites_model->get_facility_details($facility);
        $data['art_numbers_by_service'] = $this->adt_sites_model->adt_art_numbers_by_service($facility);
        $data['art_ageing']             = $this->adt_sites_model->adt_art_ageing_table($facility);
        $data['current_regimen']        = $this->adt_sites_model->adt_current_regimen($facility);
        $data['patient_regimen']        = $this->adt_sites_model->get_patient_regimen($facility);
        $data['lab_data']               = $this->adt_sites_model->get_patient_viralload($facility);
        $data['mmd_mms']                = $this->adt_sites_model->get_mmd_mms($facility);
        
        $data['title'] = 'ADT Dashboard';
        
        $this->load->view('facility_view', $data);
    }
    public function adt_art_numbers_by_status()
    {
        $data = $this->adt_sites_model->adt_art_numbers_by_status();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function art_numbers_by_service()
    {
        $data = $this->adt_sites_model->adt_art_numbers_by_service();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function get_patients_by_partner()
    {
        $data = $this->adt_sites_model->get_patients_by_partner();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function get_regimen_per_patient()
    {
        $data = $this->adt_sites_model->get_regimen_per_patient();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function adt_art_ageing_table()
    {
        $data = $this->adt_sites_model->adt_art_ageing_table();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}