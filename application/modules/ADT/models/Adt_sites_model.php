<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adt_sites_model extends CI_Model
{   
    public function get_adt_backups()
    {
        $query_str    = "SELECT * FROM vw_get_adt_backups";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();
        
        return $result_array;
    }
    public function get_adt_backups_versions()
    {
        $query_str = "SELECT * FROM vw_get_adt_backups_versions";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;
    }
    public function get_no_backup()
    { 
        $query_str = "SELECT * FROM vw_no_adt_backup";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;
    }
    public function get_adt_sites_distribution_numbers()
    {
        $columns   = array();
        $query_str = "SELECT * FROM vw_get_adt_sites_distribution_numbers";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();
        
        return $result_array;    
    }
    public function adt_art_ageing_table($facility = null)
    {
        $cond = (isset($facility)) ? $cond . "and facility = $facility" : "";
        
        $query_str = "SELECT 
        CASE
        WHEN FLOOR(DATEDIFF(now(),enrollment_date)/365) < 1 THEN 'Below 1' 
        WHEN FLOOR(DATEDIFF(now(),enrollment_date)/365) > 0 AND  FLOOR(DATEDIFF(now(),enrollment_date)/365) < 4 THEN '1 - 3 years' 
        WHEN FLOOR(DATEDIFF(now(),enrollment_date)/365) > 2 AND  FLOOR(DATEDIFF(now(),enrollment_date)/365) < 6 THEN '3 - 5 years'
        WHEN FLOOR(DATEDIFF(now(),enrollment_date)/365) > 4 AND  FLOOR(DATEDIFF(now(),enrollment_date)/365) < 8 THEN '5 - 7 years'
        WHEN FLOOR(DATEDIFF(now(),enrollment_date)/365) > 6 AND  FLOOR(DATEDIFF(now(),enrollment_date)/365) < 10 THEN '8 - 9 years'
        ELSE 'N/A' END AS duration, count(ccc_number) as total_patients
        FROM dsh_patient_adt
        WHERE enrollment_date IS NOT NULL AND enrollment_date != '0000-00-00'
        AND lower(status) = 'active' $cond
        group by duration";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;  
    }
    public function adt_art_numbers_by_status($facility = null)
    {
        $cond = (isset($facility)) ? $cond . "and facility = $facility" : "";
        
        $query_str = "SELECT * FROM vw_adt_art_numbers_by_status"; 
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();
        
        return $result_array;
    }
    public function adt_art_numbers_by_service($facility = null)
    {
        $cond      = (isset($facility)) ? $cond . "and facility = $facility" : "";
        $query_str = "SELECT service, 
                        sum(case when gender='male' then 1 else 0 end) as male,
                        sum(case when gender='female' then 1 else 0 end) as female, 
                        sum(case when gender is null then 1 else 0 end) as no_gender, 
                        count(service) as total_patients
                                FROM dsh_patient_adt
                                where service IS NOT NULL and service != '' $cond and status like '%active%'
                                group by service
                                union
                        select 'Total Patients by service' as total_patients, 
                        sum(gender='male'),
                        sum(gender='female'),
                        sum(gender is null),
                        count(service)
                        from  dsh_patient_adt
                        WHERE service IS NOT NULL and service != '' $cond and status like '%active%'
                        order by total_patients desc";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();
        
        return $result_array;  
    }
    public function adt_current_regimen($facility = null)
    {
        $cond      = (isset($facility)) ? $cond . "and facility = $facility" : "";
        $query_str = "SELECT current_regimen, sum(CASE WHEN gender='male' THEN 1 ELSE 0 END) as male, sum(case WHEN gender='female' THEN 1 ELSE 0 END) as female, count(current_regimen) as total_patients
        FROM dsh_patient_adt
        where current_regimen IS NOT NULL and current_regimen != ''  $cond and status like '%active%'
        group by current_regimen order by count(current_regimen) desc";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();
        
        return $result_array;
    }
    public function get_facility_details($facility)
    {
        $query_str = "SELECT * FROM tbl_facility f
        left join tbl_install i on f.id = i.facility_id
        WHERE f.mflcode = $facility ";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array[0];
    }
    public function get_patients_by_regimen()
    {
        $cond = (isset($facility)) ? $cond . "and facility = $facility" : "";
        
        $query_str = "SELECT * FROM vw_get_patients_by_regimen";
    }
    public function get_patients_by_county()
    {
        $cond = (isset($facility)) ? $cond . "and facility = $facility" : "";
        
        $query_str = "SELECT * FROM vw_patient_per_county_adt";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;
    }   
    public function get_patients_by_subcounty()
    {
        $cond = (isset($facility)) ? $cond . "and facility = $facility" : "";
        
        $query_str = "SELECT * FROM vw_get_patients_by_subcounty";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;
    }
    public function get_patients_by_partner()
    {
        $query_str = "SELECT * FROM vw_get_patients_by_partner";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;
    }
    public function get_regimen_per_patient()
    {
        $query_str = "SELECT * FROM vw_patients_per_regimen";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;
    }
    public function get_patient_regimen($facility = null)
    {
        $cond = (isset($facility)) ? $cond . "facility = $facility" : "";
        
        $query_str = "SELECT ccc_number, gender, current_weight, Age, current_regimen, service FROM vw_get_patient_regimen where $cond order by current_regimen asc";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();
        
        return $result_array;
    }
    public function get_patient_viralload($facility = null)
    {
        $url           = "https://viralload.nascop.org/vlapi.php?mfl=" . $facility;
        $patient_tests = array();
        $ch            = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, 0);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2);
        $json_data = curl_exec($ch);
        $data      = json_decode($json_data, TRUE);
        $lab_data  = $data['posts'];
        
        return $lab_data;
    }
    public function get_partner_service($partner)
    {
        $query_str = "SELECT * FROM vw_get_partner_service";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;
    }
    public function get_mmd_mms($facility = null)
    {
        $cond = (isset($facility)) ? $cond . "facility = $facility" : "";

        $query_str = "SELECT * FROM dsh_master_visit_mmd_mms where $cond";
        $query        = $this->db->query($query_str);
        $result_array = $query->result_array();

        return $result_array;
    }
}