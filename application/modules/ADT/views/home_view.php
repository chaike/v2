<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <!-- BEGIN: Head-->
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
      <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
      <meta name="author" content="ThemeSelect">
      <title><?=$title;?></title>
      <link rel="apple-touch-icon" href="<?= base_url();?>assets/images/ico/apple-icon-120.png">
      <link rel="shortcut icon" type="image/x-icon" href="<?= base_url();?>assets/images/ico/favicon.ico">
      <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
      <!-- BEGIN: Vendor CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/vendors.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/forms/toggle/switchery.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/plugins/forms/switch.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/colors/palette-switch.min.css">
      <!-- END: Vendor CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/tables/datatable/datatables.min.css">
      <!-- BEGIN: Theme CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/bootstrap-extended.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/colors.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/components.min.css">
      <!-- END: Theme CSS-->
      <!-- BEGIN: Page CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/menu/menu-types/horizontal-menu.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/colors/palette-gradient.min.css">
      <!-- END: Page CSS-->
      <!-- BEGIN: Custom CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/style.css">
      <!-- END: Custom CSS-->
      <!--<script src="https://code.highcharts.com/highcharts.js"></script> -->
      <script src="https://code.highcharts.com/stock/highstock.js"></script> 
      <script src="https://code.highcharts.com/highcharts-more.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>
      <style type="text/css">        
         .table td, .table th{
         padding: 5px;
         },
         #loader { 
         border: 12px solid #f3f3f3; 
         border-radius: 50%; 
         border-top: 12px solid #444444; 
         width: 70px; 
         height: 70px; 
         animation: spin 1s linear infinite; 
         } 
         @keyframes spin { 
         100% { 
         transform: rotate(360deg); 
         } 
         } 
         .center { 
         position: absolute; 
         top: 0; 
         bottom: 0; 
         left: 0; 
         right: 0; 
         margin: auto; 
         } 
      </style>
   </head>
   <!-- END: Head-->
   <!-- BEGIN: Body-->
   <body class="horizontal-layout horizontal-menu 2-columns  " data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
      <?php $this->load->view('template_menu');?>
      <!-- BEGIN: Content-->
      <div class="app-content content">
         <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
               <div class="content-header-left col-md-4 col-12 mb-2">
                  <h3 class="content-header-title">ART Analytics</h3>
               </div>
               <div class="content-header-right col-md-8 col-12">
                  <div class="breadcrumbs-top float-md-right">
                     <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>
            <div class="content-body">
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header">
                           <h4 class="card-title">Patients by status</h4>
                           <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                           <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                 <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                 <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                 <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                 <li><a data-action="close"><i class="ft-x"></i></a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="card-content collapse show">
                           <div class="card-body">
                              <p class="card-text"> </p>
                              <div id="status" style="width:100%; height:400px;">
                                 <img style="width: 120px;margin-left:50%" id="loadingDiv" src="<?php echo base_url().'assets/images/loading_spin.gif' ?>"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">ART Patients Services</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                           <ul class="list-inline mb-0">
                              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                              <li><a data-action="close"><i class="ft-x"></i></a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="card-content collapse show">
                        <div class="card-body">
                           <p class="card-text"> </p>
                           <div id="service" style="width:100%; height:400px;">
                              <img style="width: 120px;margin-left:50%" id="loadingDiv" src="<?php echo base_url().'assets/images/loading_spin.gif' ?>"/>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--/ Multi-column ordering table -->
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Number of patients per regimen</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                           <ul class="list-inline mb-0">
                              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                              <li><a data-action="close"><i class="ft-x"></i></a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                           <p class="card-text"></p>
                           <div id="regimen" style="width:100%; height:400px;">
                              <img style="width: 120px;margin-left:50%" id="loadingDiv" src="<?php echo base_url().'assets/images/loading_spin.gif' ?>"/>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--/ Multi-column ordering table -->
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Duration on treatment</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                           <ul class="list-inline mb-0">
                              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                              <li><a data-action="close"><i class="ft-x"></i></a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                           <p class="card-text"></p>
                           <div id="duration" style="width:100%; height:400px;">
                              <img style="width: 120px;margin-left:50%" id="loadingDiv" src="<?php echo base_url().'assets/images/loading_spin.gif' ?>"/>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
             <!-- Multi-column ordering table -->
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Partner Vs Patients</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                           <ul class="list-inline mb-0">
                              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                              <li><a data-action="close"><i class="ft-x"></i></a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                           <p class="card-text"></p>
                           <div id="partner" style="width:100%; height:400px;">
                              <img style="width: 120px;margin-left:50%" id="loadingDiv" src="<?php echo base_url().'assets/images/loading_spin.gif' ?>"/>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      </div>
      <!-- END: Content-->  
      <!-- BEGIN: Footer-->
      <footer class="footer footer-static footer-light navbar-shadow">
         <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">2020  &copy; </span>
            <ul class="list-inline float-md-right d-block d-md-inline-blockd-none d-lg-block mb-0">
               <li class="list-inline-item"><a class="my-1" href="" target="_blank"> - </a></li>
               <li class="list-inline-item"><a class="my-1" href="" target="_blank"> - </a></li>
            </ul>
         </div>
      </footer>
      <!-- END: Footer-->
      <!-- BEGIN: Vendor JS-->
      <script src="<?= base_url();?>assets/vendors/js/vendors.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/vendors/js/forms/toggle/switchery.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/scripts/forms/switch.min.js" type="text/javascript"></script>
      <!-- BEGIN Vendor JS-->
      <!-- BEGIN: Page Vendor JS-->
      <script type="text/javascript" src="<?= base_url();?>assets/vendors/js/ui/jquery.sticky.js"></script>
      <!-- END: Page Vendor JS-->
      <!-- BEGIN: Theme JS-->
      <script src="<?= base_url();?>assets/js/core/app-menu.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/core/app.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/scripts/customizer.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/vendors/js/jquery.sharrre.js" type="text/javascript"></script>
      <!-- END: Theme JS-->
      <script src="<?= base_url();?>assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
      <script type="text/javascript">
         $.ajax({
            url: "<?php echo base_url().'ADT/dashboard/adt_art_numbers_by_status/';?>",
            type: 'GET',
            dataType: 'json',
            success: function(data) {
               var x = [];
               var y = [];
               for(let status of data){
                  x.push(status.status);
                  y.push(parseInt(status.total_patients));
               }
         
               var myChart = Highcharts.chart('status', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'ART numbers by status'
                    },
                    xAxis: {
                        categories: x
                    },
                    yAxis: {
                        title: {
                            text: 'Number of patients'
                        }
                    },
                    plotOptions: {
                             column: {
                                 dataLabels: {
                                     enabled: true
                                 },
                                 enableMouseTracking: false
                             }
                         },
                    series:[{
                         name: 'Total',
                         data: y
                     }]
                });
            }
         });
         
      </script>
      <script type="text/javascript">
         $.ajax({
            url: "<?php echo base_url().'ADT/dashboard/art_numbers_by_service/';?>",
            type: 'GET',
            dataType: 'json',
            success: function(data) {
               var x = [];
               var ya = [];
               var yb = [];
               var yc = [];
               var yd = [];
               for(let service of data){
                  x.push(service.service);
                  ya.push(parseInt(service.male));
                  yb.push(parseInt(service.female));
                  yc.push(parseInt(service.no_gender));
                  yd.push(parseInt(service.total_patients));
               }
         
               var myChart = Highcharts.chart('service', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'ART numbers by service'
                    },
                    xAxis: {
                        categories: x
                    },
                    yAxis: {
                        title: {
                            text: 'Number of patients'
                        }
                    },
                    plotOptions: {
                             column: {
                                 dataLabels: {
                                     enabled: true
                                 },
                                 enableMouseTracking: false
                             }
                         },
                     series:[{
                         name: 'Male',
                         data: ya
                     },
                     {  name: 'Female',
                        data: yb
                     },
                     {  name: 'No Gender',
                        data: yc
                     },
                     {  name: 'Total Patients',
                        data: yd
                     }]
                });
            }
         }); 
      </script>
      <script type="text/javascript">
         $.ajax({
            url: "<?php echo base_url().'ADT/dashboard/get_regimen_per_patient/';?>",
            type: 'GET',
            dataType: 'json',
            success: function(data) {
               var x = [];
               var ya = [];
               var yb = [];
               var yc = [];
               var yd = [];
               for(let current_regimen of data){
                  x.push(current_regimen.current_regimen);
                  ya.push(parseInt(current_regimen.male));
                  yb.push(parseInt(current_regimen.female));
                  yc.push(parseInt(current_regimen.no_gender));
                  yd.push(parseInt(current_regimen.total));
               }
         
               var myChart = Highcharts.chart('regimen', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Number of patients per regimen'
                    },
                    xAxis: {
                        categories: x,
                        min: 0,
                        max: 5
                    },
                        scrollbar: {
                        enabled: true
                   },
                    yAxis: {
                        title: {
                            text: 'Number of patients'
                        }
                    },
                    plotOptions: {
                             column: {
                                 dataLabels: {
                                     enabled: true
                                 },
                                 enableMouseTracking: false
                             }
                    },
                    series:[{
                         name: 'Male',
                         data: ya
                     },
                     {  name: 'Female',
                        data: yb
                     },
                     {  name: 'No Gender',
                        data: yc
                     },
                     {  name: 'Total Patients',
                        data: yd
                     }]
                });
            }
         });    
      </script>
      <script type="text/javascript">
         $.ajax({
            url: "<?php echo base_url().'ADT/dashboard/adt_art_ageing_table/';?>",
            type: 'GET',
            dataType: 'json',
            success: function(data) {
               var x = [];
               var y = [];
               for(let duration of data){
                  x.push(duration.duration);
                  y.push(parseInt(duration.total_patients));
               }
         
               var myChart = Highcharts.chart('duration', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Duration of patients on treatment'
                    },
                    xAxis: {
                        categories: x
                    },
                    yAxis: {
                        title: {
                            text: 'Number of patients'
                        }
                    },
                    plotOptions: {
                             column: {
                                 dataLabels: {
                                     enabled: true
                                 },
                                 enableMouseTracking: false
                             }
                         },
                    series:[{
                         name: 'Total',
                         data: y
                     }]
                });
            }
         });
         
      </script>
      <script type="text/javascript">
         $.ajax({
            url: "<?php echo base_url().'ADT/dashboard/get_patients_by_partner/';?>",
            type: 'GET',
            dataType: 'json',
            success: function(data) {
               var x = [];
               var y = [];
               for(let partner of data){
                  x.push(partner.partner);
                  y.push(parseInt(partner.active_art_total));
               }
         
               var myChart = Highcharts.chart('partner', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'ART patient partner distribution'
                    },
                    xAxis: {
                        categories: x,
                        min: 0,
                        max: 10
                    },
                        scrollbar: {
                        enabled: true
                   },
                    yAxis: {
                        title: {
                            text: 'Number of patients'
                        }
                    },
                    plotOptions: {
                             column: {
                                 dataLabels: {
                                     enabled: true
                                 },
                                 enableMouseTracking: false
                             }
                    },
                    series:[{
                         name: 'Total',
                         data: y
                     }]
                });
            }
         });
         
      </script>
      <script>
         $(window).load(function() {
           $('#loadingDiv').hide();
         });
      </script>
      <!-- BEGIN: Page JS-->
      <!-- END: Page JS-->
   </body>
   <!-- END: Body-->
</html>