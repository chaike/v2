<!-- BEGIN: Main Menu-->
<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
   <div class="navbar-container main-menu-content" data-menu="menu-container">
      <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
         <!--<li class="dropdown nav-item" ><a class=" nav-link" href="<?= base_url().'ADT';?>"><i class="ft-home"></i><span>ADT Dashboard</span></a>-->
         <li class="dropdown nav-item" ><a class=" nav-link" href="<?= base_url().'ADT/facilities';?>"><i class="ft-cloud"></i><span>WEB ADT Installed Dashboard</span></a> 
         <li class="dropdown nav-item" ><a class=" nav-link" href="<?= base_url().'ADT/partner';?>"><i class="ft-cloud"></i><span>Partner</span></a>    
         </li>
         <li class="dropdown nav-item" ><a class=" nav-link" href="<?= base_url().'ADT/county';?>"><i class="ft-cloud"></i><span>County</span></a>    
         </li>
         <li class="dropdown nav-item" ><a class=" nav-link" href="<?= base_url().'ADT/subcounty';?>"><i class="ft-cloud"></i><span>Sub County</span></a>   
         <li class="dropdown nav-item" ><a class=" nav-link" href="<?= base_url().'ADT/backups';?>"><i class="ft-cloud"></i><span>Backups</span></a>   
         </li>
      </ul>
   </div>
</div>
<!-- END: Main Menu-->