<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <!-- BEGIN: Head-->
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
      <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code." />
      <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard" />
      <meta name="author" content="ThemeSelect" />
      <title><?=$title;?></title>
      <link rel="apple-touch-icon" href="<?= base_url();?>assets/images/ico/apple-icon-120.png" />
      <link rel="shortcut icon" type="image/x-icon" href="<?= base_url();?>assets/images/ico/favicon.ico" />
      <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet" />
      <!-- BEGIN: Vendor CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/vendors.min.css" />
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/forms/toggle/switchery.min.css" />
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/plugins/forms/switch.min.css" />
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/colors/palette-switch.min.css" />
      <!-- END: Vendor CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/tables/datatable/datatables.min.css" />
      <!-- BEGIN: Theme CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/bootstrap.min.css" />
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/bootstrap-extended.min.css" />
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/colors.min.css" />
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/components.min.css" />
      <!-- END: Theme CSS-->
      <!-- BEGIN: Page CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/menu/menu-types/horizontal-menu.min.css" />
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/colors/palette-gradient.min.css" />
      <!-- END: Page CSS-->
      <!-- BEGIN: Custom CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/style.css" />
      <!-- END: Custom CSS-->
      <style type="text/css">
         .table td,
         .table th {
         padding: 5px;
         }
      </style>
   </head>
   <!-- END: Head-->
   <!-- BEGIN: Body-->
   <body class="horizontal-layout horizontal-menu 2-columns" data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
      <?php $this->load->view('template_menu');?>
      <div class="app-content content">
         <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
               <div class="content-header-left col-md-4 col-12 mb-2">
                  <h3 class="content-header-title">Facility</h3>
               </div>
               <div class="content-header-right col-md-8 col-12">
                  <div class="breadcrumbs-top float-md-right">
                     <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                           <li class="breadcrumb-item"><a href="#">county</a></li>
                           <li class="breadcrumb-item active"><?= $facility['name'];?></li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>
            <div class="content-detached content-right">
               <div class="content-body">
                  <section class="row">
                     <div class="col-6">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Duration on treatment</h4>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                 <ul class="list-inline mb-0">
                                    <li>
                                       <a data-action="collapse"><i class="ft-minus"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="reload"><i class="ft-rotate-cw"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="expand"><i class="ft-maximize"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="close"><i class="ft-x"></i></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="card-content collapse show">
                              <div class="card-body">
                                 <p class="card-text"></p>
                                 <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover mb-0">
                                       <caption>
                                          number of patients on ART grouped by year duration
                                       </caption>
                                       <thead class="bg-primary white">
                                          <tr>
                                             <th scope="col">Duration</th>
                                             <th scope="col">Patients</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php foreach ($art_ageing as $p) { ?>
                                          <tr>
                                             <td><?= $p['duration'];?></td>
                                             <td><?= number_format($p['total_patients']);?></td>
                                          </tr>
                                          <?php } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">ART Patients Services</h4>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                 <ul class="list-inline mb-0">
                                    <li>
                                       <a data-action="collapse"><i class="ft-minus"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="reload"><i class="ft-rotate-cw"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="expand"><i class="ft-maximize"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="close"><i class="ft-x"></i></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="card-content collapse show">
                              <div class="card-body">
                                 <p class="card-text"></p>
                                 <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover mb-0">
                                       <caption>
                                          number of patients on ART grouped by Service
                                       </caption>
                                       <thead class="bg-primary white">
                                          <tr>
                                             <th scope="col">Service</th>
                                             <th scope="col">Patients</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php foreach ($art_numbers_by_service as $p) { ?>
                                          <tr>
                                             <td><?= $p['service'];?></td>
                                             <td><?= number_format($p['total_patients']);?></td>
                                          </tr>
                                          <?php } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
                  <div class="row">
                     <div class="col-6">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Current regimen totals</h4>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                 <ul class="list-inline mb-0">
                                    <li>
                                       <a data-action="collapse"><i class="ft-minus"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="reload"><i class="ft-rotate-cw"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="expand"><i class="ft-maximize"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="close"><i class="ft-x"></i></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="card-content collapse show">
                              <div class="card-body">
                                 <p class="card-text"></p>
                                 <div class="table-responsive">
                                    <table id="multivariable" class="table table-bordered table-striped table-hover mb-0">
                                       <caption>
                                          Table showing total numbers of patients per regimen
                                       </caption>
                                       <thead class="bg-primary white">
                                          <tr>
                                             <th scope="col">Current Regimen</th>
                                             <th scope="col">Male</th>
                                             <th scope="col">Female</th>
                                             <th scope="col">Totals</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php foreach ($current_regimen as $t) { ?>
                                          <tr>
                                             <td><?=$t['current_regimen'];?></td>
                                             <td><?=$t['male'];?></td>
                                             <td><?=$t['female'];?></td>
                                             <td><?=$t['total_patients'];?></td>
                                          </tr>
                                          <?php } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Viral load results</h4>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                 <ul class="list-inline mb-0">
                                    <li>
                                       <a data-action="collapse"><i class="ft-minus"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="reload"><i class="ft-rotate-cw"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="expand"><i class="ft-maximize"></i></a>
                                    </li>
                                    <li>
                                       <a data-action="close"><i class="ft-x"></i></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="card-content collapse show">
                              <div class="card-body">
                                 <p class="card-text"></p>
                                 <div class="table-responsive">
                                    <table id="multiviral" class="table table-bordered table-striped table-hover mb-0">
                                       <caption>
                                          Table viral load data
                                       </caption>
                                       <thead class="bg-primary white">
                                          <tr>
                                             <th scope="col">ID</th>
                                             <th scope="col">Patient</th>
                                             <th scope="col">datecollected</th>
                                             <th scope="col">DateTested</th>
                                             <th scope="col">Result</th>
                                             <th scope="col">Justification</th>
                                             <th scope="col">MFLCode</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php foreach($lab_data as $lab){ ?>
                                          <?php foreach($lab as $tests){ ?>
                                          <tr>
                                             <td><?=$tests['ID'];?></td>
                                             <td><?=$tests['Patient'];?></td>
                                             <td><?=$tests['datecollected'];?></td>
                                             <td><?=$tests['DateTested'];?></td>
                                             <td><?=$tests['Result'];?></td>
                                             <td><?=$tests['Justification'];?></td>
                                             <td><?=$tests['MFLCode'];?></td>
                                          </tr>
                                          <?php  } ?>
                                          <?php  } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header">
                           <h4 class="card-title">Patient by regimen</h4>
                           <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                           <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                 <li>
                                    <a data-action="collapse"><i class="ft-minus"></i></a>
                                 </li>
                                 <li>
                                    <a data-action="reload"><i class="ft-rotate-cw"></i></a>
                                 </li>
                                 <li>
                                    <a data-action="expand"><i class="ft-maximize"></i></a>
                                 </li>
                                 <li>
                                    <a data-action="close"><i class="ft-x"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="card-content collapse show">
                           <div class="card-body">
                              <p class="card-text"></p>
                              <div class="table-responsive">
                                 <table id="multipatient" class="table table-bordered table-striped table-hover mb-0">
                                    <caption>
                                       Table showing patients per regimen
                                    </caption>
                                    <thead class="bg-primary white">
                                       <tr>
                                          <th scope="col">CCC Number</th>
                                          <th scope="col">Gender</th>
                                          <th scope="col">Current Weight</th>
                                          <th scope="col">Age</th>
                                          <th scope="col">Current Regimen</th>
                                          <th scope="col">Service</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php foreach ($patient_regimen as $u) { ?>
                                       <tr>
                                          <td><?=$u['ccc_number'];?></td>
                                          <td><?=$u['gender'];?></td>
                                          <td><?=$u['current_weight'];?></td>
                                          <td><?=$u['Age'];?></td>
                                          <td><?=$u['current_regimen'];?></td>
                                          <td><?=$u['service'];?></td>
                                       </tr>
                                       <?php } ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header">
                           <h4 class="card-title">Multi-Month Dispensing (MMD)/Multi-Month Scripting (MMS)</h4>
                           <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                           <div class="heading-elements">
                              <ul class="list-inline mb-0">
                                 <li>
                                    <a data-action="collapse"><i class="ft-minus"></i></a>
                                 </li>
                                 <li>
                                    <a data-action="reload"><i class="ft-rotate-cw"></i></a>
                                 </li>
                                 <li>
                                    <a data-action="expand"><i class="ft-maximize"></i></a>
                                 </li>
                                 <li>
                                    <a data-action="close"><i class="ft-x"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="card-content collapse show">
                           <div class="card-body">
                              <p class="card-text"></p>
                              <div class="table-responsive">
                                 <table id="mmd_mms_report" cellpadding="5" border="1" width="100%" style="border:1px solid #DDD;">
                                    <thead>
                                       <tr>
                                          <th class=""></th>
                                          <th class="" colspan="2">&lt;1</th>
                                          <th class="" colspan="2">1-4</th>
                                          <th class="" colspan="2">5-9</th>
                                          <th class="" colspan="2">10-14</th>
                                          <th class="" colspan="2">15-19</th>
                                          <th class="" colspan="2">20-24</th>
                                          <th class="" colspan="2">25-29</th>
                                          <th class="" colspan="2">30-34</th>
                                          <th class="" colspan="2">35-39</th>
                                          <th class="" colspan="2">40-44</th>
                                          <th class="" colspan="2">45-49</th>
                                          <th class="" colspan="2">50+</th>
                                          <th class="">Sub Total</th>
                                       </tr>
                                       <tr>
                                          <td class=""></td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class="">M</td>
                                          <td class="">F</td>
                                          <td class=""></td>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php foreach ($mmd_mms as $results) { ?>
                                       <tr>
                                          <td class="">1 MONTHS</td>
                                          <td class=""><?= $results['1MONEMONTH']; ?></td>
                                          <td class=""><?= $results['1FONEMONTH']; ?></td>
                                          <td class=""><?= $results['1-4MONEMONTH']; ?></td>
                                          <td class=""><?= $results['1-4MONEMONTH']; ?></td>
                                          <td class=""><?= $results['5-9MONEMONTH']; ?></td>
                                          <td class=""><?= $results['5-9FONEMONTH']; ?></td>
                                          <td class=""><?= $results['10-14MONEMONTH']; ?></td>
                                          <td class=""><?= $results['10-14FONEMONTH']; ?></td>
                                          <td class=""><?= $results['15-19MONEMONTH']; ?></td>
                                          <td class=""><?= $results['15-19FONEMONTH']; ?></td>
                                          <td class=""><?= $results['20-24MONEMONTH']; ?></td>
                                          <td class=""><?= $results['20-24FONEMONTH']; ?></td>
                                          <td class=""><?= $results['25-29MONEMONTH']; ?></td>
                                          <td class=""><?= $results['25-29FONEMONTH']; ?></td>
                                          <td class=""><?= $results['30-34MONEMONTH']; ?></td>
                                          <td class=""><?= $results['30-34FONEMONTH']; ?></td>
                                          <td class=""><?= $results['35-39MONEMONTH']; ?></td>
                                          <td class=""><?= $results['35-39FONEMONTH']; ?></td>
                                          <td class=""><?= $results['40-44MONEMONTH']; ?></td>
                                          <td class=""><?= $results['40-44FONEMONTH']; ?></td>
                                          <td class=""><?= $results['45-49MONEMONTH']; ?></td>
                                          <td class=""><?= $results['45-49FONEMONTH']; ?></td>
                                          <td class=""><?= $results['50MONEMONTH']; ?></td>
                                          <td class=""><?= $results['50FONEMONTH']; ?></td>
                                          <td class=""><?= $results['SUBTOTAL1MONTH']; ?></td>
                                       </tr>
                                       <tr>
                                          <td class="">2 MONTHS</td>
                                          <td class=""><?= $results['1MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['1FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['1-4MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['1-4MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['5-9MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['5-9FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['10-14MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['10-14FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['15-19MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['15-19FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['20-24MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['20-24FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['25-29MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['25-29FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['30-34MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['30-34FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['35-39MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['35-39FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['40-44MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['40-44FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['45-49MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['45-49FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['50MTWOMONTH']; ?></td>
                                          <td class=""><?= $results['50FTWOMONTH']; ?></td>
                                          <td class=""><?= $results['SUBTOTAL2MONTH']; ?></td>
                                       </tr>
                                       <tr>
                                          <td class="">3 MONTHS</td>
                                          <td class=""><?= $results['1MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['1FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['1-4MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['1-4FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['5-9MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['5-9FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['10-14MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['10-14FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['15-19MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['15-19FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['20-24MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['20-24FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['25-29MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['25-29FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['30-34MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['30-34FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['35-39MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['35-39FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['40-44MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['40-44FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['45-49MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['45-49FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['50MTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['50FTHREEMONTH']; ?></td>
                                          <td class=""><?= $results['SUBTOTAL3MONTH']; ?></td>
                                       </tr>
                                       <tr>
                                          <td class="">4 MONTHS</td>
                                          <td class=""><?= $results['1MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['1FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['1-4MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['1-4FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['5-9MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['5-9FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['10-14MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['10-14FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['15-19MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['15-19FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['20-24MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['20-24FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['25-29MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['25-29FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['30-34MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['30-34FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['35-39MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['35-39FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['40-44MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['40-44FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['45-49MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['45-49FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['50MFOURMONTH']; ?></td>
                                          <td class=""><?= $results['50FFOURMONTH']; ?></td>
                                          <td class=""><?= $results['SUBTOTAL4MONTH']; ?></td>
                                       </tr>
                                       <tr>
                                          <td class="">5 MONTHS</td>
                                          <td class=""><?= $results['1MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['1FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['1-4MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['1-4FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['5-9MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['5-9FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['10-14MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['10-14FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['15-19MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['15-19FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['20-24MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['20-24FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['25-29MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['25-29FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['30-34MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['30-34FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['35-39MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['35-39FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['40-44MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['40-44FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['45-49MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['45-49FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['50MFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['50FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['SUBTOTAL5MONTH']; ?></td>
                                       </tr>
                                       <tr>
                                          <td class="">6 MONTHS</td>
                                          <td class=""><?= $results['1MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['1FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['1-4MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['1-4FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['5-9MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['5-9FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['10-14MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['10-14FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['15-19MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['15-19FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['20-24MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['20-24FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['25-29MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['25-29FFIVEMONTH']; ?></td>
                                          <td class=""><?= $results['30-34MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['30-34FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['35-39MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['35-39FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['40-44MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['40-44FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['45-49MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['45-49FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['50MSIXMONTH']; ?></td>
                                          <td class=""><?= $results['50FSIXMONTH']; ?></td>
                                          <td class=""><?= $results['SUBTOTAL6MONTH']; ?></td>
                                       </tr>
                                       <tr>
                                          <td class="">Total</td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""></td>
                                          <td class=""><?= $results['TOTALMONTHS']; ?></td>
                                       </tr>
                                       <tr>
                                          <td class="">MMS</td>
                                          <td class=""><?= $results['MMSMLESS1YEAR']; ?></td>
                                          <td class=""><?= $results['MMSFLESS1YEAR']; ?></td>
                                          <td class=""><?= $results['MMS1-4M']; ?></td>
                                          <td class=""><?= $results['MMS1-4F']; ?></td>
                                          <td class=""><?= $results['MMS5-9M']; ?></td>
                                          <td class=""><?= $results['MMS5-9F']; ?></td>
                                          <td class=""><?= $results['MMS10-14M']; ?></td>
                                          <td class=""><?= $results['MMS10-14F']; ?></td>
                                          <td class=""><?= $results['MMS15-19M']; ?></td>
                                          <td class=""><?= $results['MMS15-19F']; ?></td>
                                          <td class=""><?= $results['MMS20-24M']; ?></td>
                                          <td class=""><?= $results['MMS20-24F']; ?></td>
                                          <td class=""><?= $results['MMS25-29M']; ?></td>
                                          <td class=""><?= $results['MMS25-29F']; ?></td>
                                          <td class=""><?= $results['MMS30-34M']; ?></td>
                                          <td class=""><?= $results['MMS30-34F']; ?></td>
                                          <td class=""><?= $results['MMS35-39M']; ?></td>
                                          <td class=""><?= $results['MMS35-39F']; ?></td>
                                          <td class=""><?= $results['MMS40-44M']; ?></td>
                                          <td class=""><?= $results['MMS40-44F']; ?></td>
                                          <td class=""><?= $results['MMS45-49M']; ?></td>
                                          <td class=""><?= $results['MMS45-49F']; ?></td>
                                          <td class=""><?= $results['MMSOVER50M']; ?></td>
                                          <td class=""><?= $results['MMSOVER50F']; ?></td>
                                          <td class=""><?= $results['MMSTOTAL']; ?></td>
                                       </tr>
                                       <?php } ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="sticky-wrapper" class="sticky-wrapper" style="height: 775.98px;">
               <div class="sidebar-detached sidebar-left sidebar-sticky" style="">
                  <div class="sidebar">
                     <div class="sidebar-content card d-none d-lg-block">
                        <div class="card-body">
                           <div class="category-title pb-1">
                              <h6><?= $facility['name'];?></h6>
                              <p>
                                 <?= $facility['category'];?>
                                 site
                              </p>
                              <hr />
                              <!-- List Group example -->
                              <table class="table table-bordered table-striped mb-0">
                                 <tbody>
                                    <tr>
                                       <td>MFL Code</td>
                                       <td><?= ($facility['mflcode']);?></td>
                                    </tr>
                                    <tr>
                                       <td>ADT Version</td>
                                       <td><?= ($facility['version']);?></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- BEGIN: Footer-->
      <footer class="footer footer-static footer-light navbar-shadow">
         <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">2020 &copy; </span>
            <ul class="list-inline float-md-right d-block d-md-inline-blockd-none d-lg-block mb-0">
               <li class="list-inline-item"><a class="my-1" href="" target="_blank"> - </a></li>
               <li class="list-inline-item"><a class="my-1" href="" target="_blank"> - </a></li>
            </ul>
         </div>
      </footer>
      <!-- END: Footer-->
      <!-- BEGIN: Vendor JS-->
      <script src="<?= base_url();?>assets/vendors/js/vendors.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/vendors/js/forms/toggle/switchery.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/scripts/forms/switch.min.js" type="text/javascript"></script>
      <!-- BEGIN Vendor JS-->
      <!-- BEGIN: Page Vendor JS-->
      <script type="text/javascript" src="<?= base_url();?>assets/vendors/js/ui/jquery.sticky.js"></script>
      <!-- END: Page Vendor JS-->
      <!-- BEGIN: Theme JS-->
      <script src="<?= base_url();?>assets/js/core/app-menu.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/core/app.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/scripts/customizer.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/vendors/js/jquery.sharrre.js" type="text/javascript"></script>
      <!-- END: Theme JS-->
      <script src="<?= base_url();?>assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
      <script type="text/javascript">
         $(document).ready(function () {
             // $(".zero-configuration").DataTable(),$(".default-ordering").DataTable({order:[[3,"desc"]]}),
             $(".multi-ordering").DataTable({
                 columnDefs: [
                     { targets: [0], orderData: [0, 1] },
                     { targets: [1], orderData: [1, 0] },
                     { targets: [4], orderData: [4, 0] },
                 ],
             });
         
             // ,$(".complex-headers").DataTable(),$(".dom-positioning").DataTable({dom:'<"top"i>rt<"bottom"flp><"clear">'}),$(".alt-pagination").DataTable({pagingType:"full_numbers"}),$(".scroll-vertical").DataTable({scrollY:"200px",scrollCollapse:!0,paging:!1}),$(".dynamic-height").DataTable({scrollY:"50vh",scrollCollapse:!0,paging:!1}),$(".scroll-horizontal").DataTable({scrollX:!0}),$(".scroll-horizontal-vertical").DataTable({scrollY:200,scrollX:!0}),$(".comma-decimal-place").DataTable({language:{decimal:",",thousands:"."}})
         });
         $("#multitables").DataTable({
             dom: "Bfrtip",
             // Configure the drop down options.
             lengthMenu: [
                 [10, 25, 50, -1],
                 ["10 rows", "25 rows", "50 rows", "Show all"],
             ],
             // Add to buttons the pageLength option.
             buttons: ["copy", "csv", "excel", "pdf", "print"],
             order: [[1, "desc"]],
         });
         $("#multivariable").DataTable({
             dom: "Bfrtip",
             // Configure the drop down options.
             lengthMenu: [
                 [10, 25, 50, -1],
                 ["10 rows", "25 rows", "50 rows", "Show all"],
             ],
             // Add to buttons the pageLength option.
             buttons: ["copy", "csv", "excel", "pdf", "print"],
             order: [[3, "desc"]],
         });
         $("#multipatient").DataTable({
             dom: "Bfrtip",
             // Configure the drop down options.
             lengthMenu: [
                 [10, 25, 50, -1],
                 ["10 rows", "25 rows", "50 rows", "Show all"],
             ],
             // Add to buttons the pageLength option.
             buttons: ["copy", "csv", "excel", "pdf", "print"],
             order: [[4, "asc"]], 
         });
             $("#multiviral").DataTable({
             dom: "Bfrtip",
             // Configure the drop down options.
             lengthMenu: [
                 [10, 25, 50, -1],
                 ["10 rows", "25 rows", "50 rows", "Show all"],
             ],
             // Add to buttons the pageLength option.
             buttons: ["copy", "csv", "excel", "pdf", "print"],
             order: [[3, "desc"]],
         });
            $("#mmd_mms_report").DataTable({
             dom: "Bfrtip",
             // Configure the drop down options.
             lengthMenu: [
                 [10, 25, 50, -1],
                 ["10 rows", "25 rows", "50 rows", "Show all"],
             ],
             // Add to buttons the pageLength option.
             buttons: ["copy", "csv", "excel", "pdf", "print"],
         });
      </script>
      <!-- BEGIN: Page JS-->
      <!-- END: Page JS-->
   </body>
   <!-- END: Body-->
</html>