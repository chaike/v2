<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <!-- BEGIN: Head-->
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
      <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
      <meta name="author" content="ThemeSelect">
      <title><?=$title;?></title>
      <link rel="apple-touch-icon" href="<?= base_url();?>assets/images/ico/apple-icon-120.png">
      <link rel="shortcut icon" type="image/x-icon" href="<?= base_url();?>assets/images/ico/favicon.ico">
      <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
      <!-- BEGIN: Vendor CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/vendors.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/forms/toggle/switchery.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/plugins/forms/switch.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/colors/palette-switch.min.css">
      <!-- END: Vendor CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/vendors/css/tables/datatable/datatables.min.css">
      <!-- BEGIN: Theme CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/bootstrap-extended.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/colors.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/components.min.css">
      <!-- END: Theme CSS-->
      <!-- BEGIN: Page CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/menu/menu-types/horizontal-menu.min.css">
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/core/colors/palette-gradient.min.css">
      <!-- END: Page CSS-->
      <!-- BEGIN: Custom CSS-->
      <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/style.css">
      <!-- END: Custom CSS-->
      <style type="text/css">        
         .table td, .table th{
         padding: 5px;
         }
      </style>
   </head>
   <!-- END: Head-->
   <!-- BEGIN: Body-->
   <body class="horizontal-layout horizontal-menu 2-columns  " data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
      <?php $this->load->view('template_menu');?>
      <!-- BEGIN: Content-->
      <div class="app-content content">
         <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
               <div class="content-header-left col-md-4 col-12 mb-2">
                  <h3 class="content-header-title"><?=$title ?></h3>
               </div>
               <div class="content-header-right col-md-8 col-12">
                  <div class="breadcrumbs-top float-md-right">
                     <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>
            <div class="content-body">
               <section id="multi-column">
                  <div class="row">
                     <div class="col-xl-4 col-lg-12">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">ADT backups <?= $backups_versions[0]['total'];?></h4>
                              <a class="heading-elements-toggle">
                              <i class="la la-ellipsis-v font-medium-3"></i>
                              </a>
                              <div class="heading-elements">
                                 <ul class="list-inline mb-0">
                                    <li>
                                       <a data-action="reload">
                                       <i class="ft-rotate-cw"></i>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="card-content">
                              <div class="card-body">
                                 <div class="project">
                                    <?php foreach($backups_versions as $b) { 
                                       $percentage = $b['backups'] / $b['total'] * 100; 
                                       ?>
                                    <div class="project-a mb-2">
                                       <p class="m-0 blue-grey darken-4"><?=$b['adt_version']?> ( <?= round($percentage);?>%)
                                          <span class="sucess float-right"><?= $b['backups']; ?> Facilities  </span>
                                       </p>
                                       <div class="progress mt-1 mb-0" style="height: 7px;">
                                          <div class="progress-bar bg-info" role="progressbar" style="width: <?= $percentage; ?>%" aria-valuenow="<?= $percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                       </div>
                                    </div>
                                    <?php } ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-8 col-sm-12">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Facilities Backups</h4>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                 <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="card-content collapse show">
                              <div class="card-body card-dashboard">
                                 <p class="card-text">Table showing facility backups</p>
                                 <div class="table-responsive">
                                    <table id="table-list" class="table table-striped table-bordered ">
                                       <thead>
                                          <tr>
                                             <th>Facility</th>
                                             <th>Partner</th>
                                             <th>MFL Code</th>
                                             <th>ADT Version</th>
                                             <th>Last Backup</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php foreach ($backups as $b){?>
                                          <tr>
                                             <td><?=$b['facility'];?></td>
                                             <td><?=$b['partner'];?></td>
                                             <td><?=$b['mfl_code'];?></td>
                                             <td><?=$b['adt_version'];?></td>
                                             <td><?=$b['last_backup'];?></td>
                                          </tr>
                                          <?php } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-8 col-sm-12">
                        <div class="card">
                           <div class="card-header">
                              <h4 class="card-title">Facilities Without Backups</h4>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                 <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="card-content collapse show">
                              <div class="card-body card-dashboard">
                                 <p class="card-text">Table showing facilities without backup</p>
                                 <div class="table-responsive">
                                    <table id="no-backup" class="table table-striped table-bordered ">
                                       <thead>
                                          <tr>
                                             <th>Facility</th>
                                             <th>MFL Code</th>
                                             <th>Category</th>
                                             <th>County</th>
                                             <th>Subcounty</th>
                                             <th>Partner</th>
                                             <th>ADT Version</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php foreach ($no_backups as $b){?>
                                          <tr>
                                             <td><?=$b['facility'];?></td>
                                             <td><?=$b['mflcode'];?></td>
                                             <td><?=$b['category'];?></td>
                                             <td><?=$b['county'];?></td>
                                             <td><?=$b['sub_county'];?></td>
                                             <td><?=$b['partner'];?></td>
                                             <td><?=$b['adt_version'];?></td>
                                          </tr>
                                          <?php } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
      <!-- END: Content-->  
      <!-- BEGIN: Footer-->
      <footer class="footer footer-static footer-light navbar-shadow">
         <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">2020  &copy; </span>
            <ul class="list-inline float-md-right d-block d-md-inline-blockd-none d-lg-block mb-0">
               <li class="list-inline-item"><a class="my-1" href="" target="_blank"> - </a></li>
               <li class="list-inline-item"><a class="my-1" href="" target="_blank"> - </a></li>
            </ul>
         </div>
      </footer>
      <!-- END: Footer-->
      <!-- BEGIN: Vendor JS-->
      <script src="<?= base_url();?>assets/vendors/js/vendors.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/vendors/js/forms/toggle/switchery.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/scripts/forms/switch.min.js" type="text/javascript"></script>
      <!-- BEGIN Vendor JS-->
      <!-- BEGIN: Page Vendor JS-->
      <script type="text/javascript" src="<?= base_url();?>assets/vendors/js/ui/jquery.sticky.js"></script>
      <!-- END: Page Vendor JS-->
      <!-- BEGIN: Theme JS-->
      <script src="<?= base_url();?>assets/js/core/app-menu.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/core/app.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/js/scripts/customizer.min.js" type="text/javascript"></script>
      <script src="<?= base_url();?>assets/vendors/js/jquery.sharrre.js" type="text/javascript"></script>
      <!-- END: Theme JS-->
      <script src="<?= base_url();?>assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
      <script type="text/javascript">
         $(document).ready(function(){
         // $(".zero-configuration").DataTable(),$(".default-ordering").DataTable({order:[[3,"desc"]]}),
                  $("#table-list").DataTable({
            dom: 'Bfrtip',
             // Configure the drop down options.
             lengthMenu: [
                 [ 10, 25, 50, -1 ],
                 [ '10 rows', '25 rows', '50 rows', 'Show all' ]
             ],
             // Add to buttons the pageLength option.
             buttons: [
                 'copy', 'csv', 'excel', 'pdf', 'print'
             ],
             order: [
             [3, 'desc']
           ]
         });
                  $("#no-backup").DataTable({
            dom: 'Bfrtip',
             // Configure the drop down options.
             lengthMenu: [
                 [ 10, 25, 50, -1 ],
                 [ '10 rows', '25 rows', '50 rows', 'Show all' ]
             ],
             // Add to buttons the pageLength option.
             buttons: [
                 'copy', 'csv', 'excel', 'pdf', 'print'
             ],
             order: [
             [6, 'desc']
           ]
         });
         });
      </script>
      <!-- BEGIN: Page JS-->
      <!-- END: Page JS-->
   </body>
   <!-- END: Body-->
</html>